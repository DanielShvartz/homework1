#include "linkedList.h"

/*
The function pushes a node into the start of the stack
input; the pointer to the head of the stack, and a value
output; will add a new node to the head of the stack
*/
void push(stack** head, int value)
{
	stack* node = new stack; // create new node
	if ((*head) == nullptr) // if the head is empty
	{
		node->_next = nullptr; // send to next to be null
		node->_value = value; // add the value
		*head = node; // and make him the first of the stack
	}
	else // if there is more that 1 value
	{
		node->_next = *head; // make the new one point to the first
		node->_value = value; // add the value
		*head = node; // make the new one be the first
	}
}

/*
The function pops a value from the top of the stack
input; the pointer to the head of the stack
output; returns the top of the stack and delets him
*/
int pop(stack** head)
{
	stack* curr = *head;
	if (curr == nullptr) // if the head is empty we return -1
		return -1;
	else if (curr->_next == nullptr) // if we have only 1 value in the stack
	{
		delete(*head); 
		return curr->_value; // return the value
	}
	else
	{
		*head = (*head)->_next; // make the second one be the top
		stack* returned_value = curr; // create returned value
		delete(curr); // delete the head
		return returned_value->_value; // return the value
	}
}