#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/* a stack contains positive integer values. */
typedef struct stack
{
	struct stack* _next;
	int _value;
} stack;

void push(stack** head, int value);
int pop(stack** head);

#endif 
