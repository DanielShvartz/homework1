#include "stack.h"
#include "utils.h"
#include <iostream>

void reverse(int* nums, unsigned int size)
{
	stack* s = new stack; // create the stack
	initStack(s); // create memory space for the stack
	for (int i = 0; i < size; i++) // add the number from the array to the stack
		push(s, nums[i]);
	for (int i = 0; i < size; i++) // pop the stack into the array
		nums[i] = pop(s);
	cleanStack(s); // clean the stack
}

/*
The function asks from the user to get 10 numbers
input; saves the numbers in an array
output; return the array with reversed numbers 
*/
int* reverse10()
{
	int* arr = new int[10];
	for (int i = 0; i < 10; i++)
		std::cin >> arr[i];
	reverse(arr, 10);
	return arr;
}