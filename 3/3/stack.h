#ifndef STACK_H
#define STACK_H

/* a positive-integer value stack, with no size limit */
typedef struct node
{
	struct stack* _next; // moving the to next one that points to a stack member
	int _value; // the value
} node;
typedef struct stack
{
	node* ptr; // created a pointer to the struct i cant use a pointer to pointer
} stack;

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty

void initStack(stack* s);
void cleanStack(stack* s);

#endif // STACK_H