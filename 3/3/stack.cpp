#include "stack.h"

/*
The function pushes a node into the start of the stack
input;the head of the stack, and a value
output; will add a new node to the head of the stack
*/

void push(stack* s, unsigned int element)
{
	stack* new_Stack_Node = new stack;
	new_Stack_Node->ptr = new node;

	if (s->ptr == nullptr) // if the head is empty
	{
		new_Stack_Node->ptr->_next = nullptr; // send to next to be null
		new_Stack_Node->ptr->_value = element; // add the value
		s = new_Stack_Node; // and make him the first of the stack
	}
	else // if there is more that 1 value
	{
		new_Stack_Node->ptr->_next = s; // make the new one point to the first
		new_Stack_Node->ptr->_value = element; // add the value
		s = new_Stack_Node; // make the new one be the first
	}
}

/*
The function pops a value from the top of the stack
input; the head of the stack
output; returns the top of the stack and deletes him
*/
int pop(stack* s) // Return -1 if stack is empty
{
	stack* curr = s;
	if (curr == nullptr) // if the head is empty we return -1
		return -1;
	else if (curr->ptr->_next == nullptr) // if we have only 1 value in the stack
	{
		delete(s);
		return curr->ptr->_value; // return the value
	}
	else
	{
		s = s->ptr->_next; // make the second one be the first
		stack* returnedStack = curr; // remember the first
		returnedStack->ptr->_value = curr->ptr->_value; // save the value
		delete(curr); // delete the first
		return returnedStack->ptr->_value; // return the value
	}

}

/*
The function initializes the stack if empty
input; stack
output; initialize him
*/
void initStack(stack* s)
{
	s->ptr = new node;
	s->ptr->_next = nullptr;
	s->ptr->_value = 0;
}

/*
The function cleans the stack from the memory

*/
void cleanStack(stack* s)
{
	stack* temp = nullptr;
	stack* curr = s;
	while (curr) // run until the end
	{
		temp = curr; // set the current
		curr = (curr)->ptr->_next; // set the next
		delete(temp->ptr); // free the pointer
		delete(temp); // free the node
	}
	s = nullptr;
}