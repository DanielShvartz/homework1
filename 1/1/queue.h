#ifndef QUEUE_H
#define QUEUE_H

/* a queue contains positive integer values. */
typedef struct queue
{
	int* _arr; // array which holds the numbers
	int _maxSize; 
	int _placeLeft; // indicates how much space left on the array
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);
void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q);

bool isFull(queue* q);
#endif /* QUEUE_H */