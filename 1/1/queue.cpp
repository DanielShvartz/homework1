#include "queue.h"
#include <iostream>

/*
The function initialize an empty queue
input; queue, and a max size
output; initializes an empty queue
*/
void initQueue(queue* q, unsigned int size)
{
	q->_arr = new int[size]; // when creating an array it has garbage values
	q->_maxSize = size; // setting size to knwo how much to run in loops
	for (int i = 0; i < size; i++) 
		q->_arr[i] = -1; // setting all the values to be -1 so we can know where located number
	q->_placeLeft = size;
}

/*
The the function cleans all the queue
input; the queue
output; cleans him
*/
void cleanQueue(queue* q)
{
	delete(q->_arr);
	delete(q);
}

/*
The function checks if the queue is full
input; the queue
output; true if full - false if not
*/
bool isFull(queue* q)
{
	return q->_placeLeft == 0; // if there is no place left it will return true, if there is place will return false
}

/*
The function appends a value into the head of the queue
input; the queue and the new value
output; will append him to the queue
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->_placeLeft > 0) // if there is place
	{
		q->_arr[q->_maxSize - q->_placeLeft] = newValue; // add from the end
		q->_placeLeft--;
	}
	else // else print
		std::cout << "Sorry, but there is no space left in the queue. Try to remove a number." << std::endl;
}

/*
The function returns element in top of queue, or -1 if empty
input; the queue
output; the first element
*/
int dequeue(queue* q)
{
	int firstElement = q->_arr[0]; // remember the first element so we can return him later
	if (isFull(q)) // if the array is full
	{
		for (int i = 0; i < q->_maxSize - 1; i++) // we shift all the elements to the left
			q->_arr[i] = q->_arr[i + 1];
		q->_arr[q->_maxSize - 1] = -1; // and make the last element as -1
	}
	else // if not
		for (int i = 0; i < q->_maxSize - q->_placeLeft; i++) // this wont work on empty array
			q->_arr[i] = q->_arr[i + 1]; // but shift all the elemnts to the left
	
	q->_placeLeft++; // more space left
	return firstElement; // return the first element
}